<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>techwriting on The Art of Not Asking Why</title>
    <link>/tags/techwriting/</link>
    <description>Recent content in techwriting on The Art of Not Asking Why</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <lastBuildDate>Wed, 13 Apr 2022 00:00:00 -0400</lastBuildDate>
    
	<atom:link href="/tags/techwriting/index.xml" rel="self" type="application/rss+xml" />
    
    
    <item>
      <title>Technical Documentations Thoughts</title>
      <link>/2022-04-12/</link>
      <pubDate>Wed, 13 Apr 2022 00:00:00 -0400</pubDate>
      
      <guid>/2022-04-12/</guid>
      <description>&lt;p&gt;A few weeks ago, I &lt;a href=&#34;https://taonaw.com/2022-02-13/&#34;&gt;briefly mentioned&lt;/a&gt; I&amp;rsquo;m getting into technical writing at work. This has been an interesting experience. The following are some thoughts and general strategies I&amp;rsquo;ve been utilizing.&lt;/p&gt;
&lt;p&gt;I discovered that technical writing at work uses the &amp;ldquo;energy bucket&amp;rdquo; as most of my writing on this blog. It&amp;rsquo;s not that both are identical: the articles I write at work cover different subjects and they end up looking different than what ends up on TAONAW. Still, I feel I scratch my technology-writing itch for that sort of writing more often than not&lt;sup id=&#34;fnref:1&#34;&gt;&lt;a href=&#34;#fn:1&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;1&lt;/a&gt;&lt;/sup&gt;.&lt;/p&gt;
&lt;p&gt;It was clear from the start: when it comes to writing and technical writing in particular, I was going to use Emacs and org-mode. The habit is ingrained too deep to consider anything else, and besides, it&amp;rsquo;s org-mode that gave my wiki-related writing the boost I need to produce my thoughts in a clear, concise manner.&lt;/p&gt;
&lt;p&gt;The first thing I did was to make sure I have a template for the outline I needed:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-emacs-lisp&#34; data-lang=&#34;emacs-lisp&#34;&gt;&lt;span style=&#34;color:#a6e22e&#34;&gt;*&lt;/span&gt; %^{Title}
%^{KB}p

** Introduction
** Requirements
** Process
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;Pretty straightforward:&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;Prompt for a title&lt;/li&gt;
&lt;li&gt;Prompt for the KB ID number, place as property&lt;/li&gt;
&lt;li&gt;Create a heading for the introduction&lt;/li&gt;
&lt;li&gt;Create a heading for the requirements&lt;/li&gt;
&lt;li&gt;Create a heading for the process&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;The introduction is for IT personnel to get a quick idea about the article they&amp;rsquo;re looking at before committing to reading it. I try to keep it as a sentence or in a few bullet points if it&amp;rsquo;s a complex process.&lt;/p&gt;
&lt;p&gt;The requirements specify hardware, software, and knowledge required to follow the instructions. Since I cover many different articles, this can range from &amp;ldquo;have an adapter with a registered MAC address&amp;rdquo; to &amp;ldquo;have written permission from the HR department.&amp;rdquo;&lt;/p&gt;
&lt;p&gt;The process is where I write most of the text. Usually, these are numbered lists (these can break down to further numbered lists) which include step-by-step descriptions with annotated screenshots. There are quite a few things to expand on when it comes to writing the process, but these deserve a separate post. I hope to expand on them later on.&lt;/p&gt;
&lt;p&gt;Besides saving me time, the template also keeps me in check and reminds me of the proper workflow. Now and then I find myself starting to explain something I do at work, and then I get this nagging feeling of &amp;ldquo;this should be written in the wiki.&amp;rdquo; Thankfully, because Emacs is right there showing me my agenda, this is a seamless process.&lt;/p&gt;
&lt;p&gt;This is one of the rudimentary yet genius things about Emacs: you probably already use it for something, so switching quickly to write a note about what you&amp;rsquo;re actually doing is as quick as thinking it. I can&amp;rsquo;t tell you how many ideas I&amp;rsquo;ve lost before I started to use Emacs simply because explaining what I&amp;rsquo;m doing meant I had to open another program, create a new note there, give it a file name, then save and get back to what I was doing. This is how good thoughts die, lost without the chance to be remembered.&lt;/p&gt;
&lt;p&gt;When I&amp;rsquo;m done with an article, it&amp;rsquo;s time to export it out of org-mode. I found that the best way to handle that is to export the article I work on to HTML from within Emacs. I copy-paste the HTML after I open it in a browser and tweak up the formatting there. This usually includes uploading the screenshots and placing them at the right place in the article while deleting some comments Emacs still adds at the end, like the option to verify the HTML code.&lt;/p&gt;
&lt;p&gt;Exporting this way also gives me a slight &amp;ldquo;bonus&amp;rdquo;: table of contents. The TOC provides a quick list of the headers and the subheaders at the top of the article, allowing others to jump to the right point in the article without scrolling, and the ability to store the exact location of what they need as a bookmark. This can be very handy, especially in a long article.&lt;/p&gt;
&lt;p&gt;Yet another Emacs bonus: footnotes. The footnotes are exported with the links tying them back to where they&amp;rsquo;re announced in the article. This way I can expand on certain items and link to extra information if needed. Since I include footnotes in its own header (like in this post), it is also available from the TOC at the head of the article. Everything is delivered with one simple export to HTML.&lt;/p&gt;
&lt;p&gt;With Emacs org-mode at my disposal, I get to extend my knowledge in my wiki to the rest of the team easily. Whatever I write is stored in my ownwiki as well as on the team&amp;rsquo;s database. I have my org files and they have their HTML exports. It&amp;rsquo;s a win-win.&lt;/p&gt;
&lt;h2 id=&#34;footnotes&#34;&gt;Footnotes&lt;/h2&gt;
&lt;section class=&#34;footnotes&#34; role=&#34;doc-endnotes&#34;&gt;
&lt;hr&gt;
&lt;ol&gt;
&lt;li id=&#34;fn:1&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Interestingly, I&amp;rsquo;m started to get more interested in creative writing and writing flash fiction, while also getting drawn more strongly toward my old &lt;a href=&#34;https://en.wikipedia.org/wiki/Dungeons%5F%26%5FDragons&#34;&gt;D&amp;amp;D&lt;/a&gt; days - but more about this soon. &lt;a href=&#34;#fnref:1&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;/ol&gt;
&lt;/section&gt;</description>
    </item>
    
    <item>
      <title>Feburary 22 Update</title>
      <link>/2022-02-13/</link>
      <pubDate>Tue, 22 Feb 2022 00:00:00 -0500</pubDate>
      
      <guid>/2022-02-13/</guid>
      <description>&lt;p&gt;With changes at work and less energy to write here, I wanted to at least write a general update. This usually helps me to &amp;ldquo;get back in the groove&amp;rdquo; blogging-wise.&lt;/p&gt;
&lt;p&gt;At work, we&amp;rsquo;re going through a merger of sorts. It&amp;rsquo;s been in the works for quite some time, but it finally took effect at the start of this month. Because our workflows and scheduling system has changed, I find myself a bit out of sync with my routines. One of the things that came about is our technical documentations; our work wiki and knowledge base, specifically.&lt;/p&gt;
&lt;p&gt;As an Emacs org-mode user, I&amp;rsquo;ve been working on my knowledge base for years. Some of our current workflows started with this personal knowledge base on my office computer&amp;rsquo;s Emacs, or at least passed through it in one shape or form.&lt;/p&gt;
&lt;p&gt;Perhaps I have some talent for writing workflows (I would assume so since I keep being tagged to do them), but it&amp;rsquo;s Emacs (and org-mode) that brings it to light. It&amp;rsquo;s not just about writing information down. Understanding Emacs and building my own wiki in org-mode gave me the confidence to tackle bigger projects on my own, from writing simple bash scripts to &lt;a href=&#34;https://mastodon.technology/@jtr/107782245308848494&#34;&gt;troubleshooting my network connectivity last weekend&lt;/a&gt; (it&amp;rsquo;s working now, thanks to &lt;a href=&#34;https://mastodon.social/@speskk&#34;&gt;speskk&lt;/a&gt;).&lt;/p&gt;
&lt;p&gt;The interest in writing technical documentations highlighted my college days, when I was taking technical writing classes. Funny how sometimes the past catches with you, especially when you think you&amp;rsquo;re done with it.&lt;/p&gt;
&lt;p&gt;Meanwhile, I feel my writing energy for the blog has diminished a little. Not because I don&amp;rsquo;t want to write: the impulse to do so is itching as it has been for the last 20 years. There&amp;rsquo;s just a lot going on, and I feel like I can&amp;rsquo;t break it down to chunks I can express in writing, at least not yet.&lt;/p&gt;
&lt;p&gt;Almost at the same time, another part of my past seem to be making a comeback. This one is personal and uncertain, hopefully positive. The collision of the two - professional and personal - made last weekend quite thought intensive.&lt;/p&gt;</description>
    </item>
    
  </channel>
</rss>
