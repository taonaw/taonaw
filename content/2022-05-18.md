+++
title = "Of Sleepless Nights and ADHD"
author = ["JarZz"]
publishDate = 2022-05-18
lastmod = 2022-10-04T13:15:04-04:00
tags = ["life"]
draft = true
+++

I mentioned not sleeping again on [Mastodon](https://mastodon.technology/web/@jtr/108317237554246474) and got the conversation went the familiar path: Coffee, blue screens, background noises. I thought I'd expand more about this here, in case someone's interested.

<!--more-->

I started to notice I'm having sleep issues about four years ago. It's been a gradual worsening stuation, with "disturbance" patterns getting longer over time. I would have a poor night of 5 hours of sleep here and there, but I would be able to move through the day OK and be fine the next. Lately though, these days started connecting. A 4-day sleep night would connect with another poor sleep night the next, and sometimes the one after that as well.

I wake up from subtlest noises: a sheet of paper falling on the floor from the fan; a neighbor closing their door in the adjacent apartment. When you live in NYC (and near a construction site that remains functional way into the night) this does not help.

Another unexpected pattern started emerging as I got deeper into technology. I seemed to have lost the ability to shut down my brain at night.

Everyone (at least, I assume so) has their brain racing from one thought to the next when they are excited, but for me, this is can escalate to another level. I get focused on what I do to the point I forget the world around me, and I hate being distrubed. This is something I've known about myself for quite some time, but the issue became more obvious with computers. I would try to understand a line of code or something about the workings of Emacs, and get lost in it.

The upside of that is learning a whole lot in a short time. This is how I got into Emacs in the first place: for a month I had a few of these events where I simply "drank up" manuals and YouTube videos and summerize them. I loved everything I learned! The downside of this was when I realized that I can't really stop thinking of these things when it's time, like, when I try to go to sleep. Sometimes, facing with a particular problem, I wake up at 2 AM right into a brainstorm. Quite a few times these resulted in a good solution. These brainstrorms feel like waves in my head: I would drift off to sleep and then actually _feel_ my brain lighting up, like a wave, as I wake up to full consciences.

Among other tell-tell signs and encouragement from my partners, I decided to seek out a professional opinion to the possiblity of haveing ADHD. It was a very throught nurological evaluation with results that came as a little suprise to me or to anyone who knows me (or, to anyone with ADHD probably reading these lines). What was interesting was the fact of why this didn't really affect me until now. In short, the reason was that I score way above average in my ablity to break things down visually, and because apperently I have a "suprior" system to manage an organize my professional and personal life. Yep. You guessed it. Emacs org-mode.

So here we are today. There's not a whole lot I can do about sleeping better. I usually don't drink coffee after 14:00, and I know from experience that coffee doesn't affect me a whole lot anyway (if anything, it actually has the affect of calming me down, but that can be just because I'm irritated when I _don't_ have coffee, which is probably everyone else who drinks bean juice). I go to bed around 23:00 or so, and typically don't have a problem falling asleep, though the construction I mention above doesn't help much. I exercise daily (when I actually sleep that is and have the energy to do so), and I eat lean foods. While I do read in bed from my phone, I do have the red screen on and the phone is at minimal brightness. I usually read for 10 minutes before I pass out anyway. I can't do much about noise (I tried different kinds of ear plugs).

Instead, my best stragedy is to "flow" with it. If I wake up in the middle of the night and I can't go to sleep, I know I might as well power up the computer and try to commit to some geekery or a laid-back computer game (no action or explosion, the puzzle kind). This means I will stay up for a few more hours, but at least I would feel the sleep coming at around 5 in the morning and go back to back for an hour or two. Not the best, but better than not having it at all. When I have an idea and I have to try it, I go ahead. Soemtimes this means I end up journaling for over an hour, sometimes all I have to do is to record a few bullet points in Orgzly on my phone. At work, I have co-workers that understand my situation and let me "disappear" for half an hour or so to catch a quick nap, if I can. These help to recharge me enough to deal with the next couple of hour before I get the chance to sleep more fully for the night.

All in all, I don't think this is "bad" nor "good" (though I do enjoy the intensity and the passion in my brainstorms). I feel greatful that I had the ability to learn to use Emacs and org-mode to organize my life and save me from the mess it has been without them[^fn:1]. Knowing I "officially" have ADHD did not change anything for me as it seem to have for others either. To me, it's more of a headnod.

I wonder if anyone reading this post, especially deep into technology, is experiecing something similar. Let me know! I'd love to hear from you.


## Footnotes {#footnotes}

[^fn:1]: To be fair though, I always had a system in place. A wiki or a todo app or even notes in a calendar, I always had to write things down. Never did I have something so wholesome and all-inclusive as Emacs and org-mode.