+++
title = "TAONAW: The Art of Not Asking Why"
author = ["JarZz"]
layout = "single"
lastmod = 2022-10-04T13:15:10-04:00
draft = false
+++

This website is essentially free to maintain[^fn:1].

TAONAW is built with [Hugo Go](https://gohugo.io/), a free open-source static-site generator. It is designed and authored on a free Linux OS using [Emacs](https://www.gnu.org/software/emacs/) [org-mode](https://orgmode.org/) and [ox-hugo](https://ox-hugo.scripter.co/).  Bandwidth is currently supplied free of charge by GitLab.

I write posts in my free time because I want to and I enjoy doing so. No one is supporting me, sponsoring me, or otherwise paying me anything to write or create or publish any content. The goal is to keep things this way.

However, if you enjoy what you read here or otherwise feel appreciative, you can donate to TAONAW with [Liberapay](https://liberapay.com/about/). Thankfully, I am not in _need_ of this money, and I hope it stays this way.

A note about copying/using the code and other parts of this site:
I use free tools and free knowledge to build this site, and you are welcome to take and use anything you'd like, _except_ the logo. The logo of the owl and the book was not free and therefore, you may _not_ copy or re-use it. It was created by another artist, who I hired for money. If you want the name, I'll be more than happy to give it to you. Just ask.

You can also go ahead and copy the content of the posts as well, just please link back to this site.

{{< rawhtml >}}
<a href="https://liberapay.com/JTR/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>
{{</ rawhtml >}}


## **Footnotes** {#footnotes}

[^fn:1]: The domain, helpdeskheadesk.net, costs me about $15 and change per year.