+++
title = "Of Choosing to Write on the Positive"
author = ["JarZz"]
publishDate = 2022-06-14
lastmod = 2022-10-04T13:15:06-04:00
tags = ["blogging"]
draft = false
+++

One of the smart things my aunt told me growing up is to focus on the good. The bad is out there, and there's plenty of sad to go around. It's the good we should choose to focus on.

<!--more-->

This advice is not about ignoring the bad things. The idea is to put our mental focus into good memories and experiences, and not waste our energy downward spirals that drag us and those around us down.

Case in point, yesterday's post. I had an idea in the morning that later developed into a draft, but I didn't like it. Something kept me back from publishing. Looking back at it today, I understand what it was. It was a negative experience that doesn't offer much learning. In other words, besides sharing a bad experience and drag other folks down, it doesn't do much.

Meanwhile, I discovered negative experiences tend to be uncomfortably personal. I don't mean the _amount_ of how personal they get, but just that I don't feel comfortable sharing that particular personal experience. It could be because it had too much information, or perhaps because I felt putting it up might cause friction with folks who know me. Positive experiences, or experiences that contribute to learning, seldom have this nagging feeling stuck to it.

It's fine to discuss problem, don't get me wrong. But the focus should be the struggle (so you don't give up) and possible solutions (learning); in other words, the problem itself is not the focus of the discussion. Writing about a bad experience that doesn't really lead anywhere is just not that great, you know?


## Comments? {#comments}

Reply to this post on [Mastodon](https://mastodon.technology/web/@jtr/108479036885841375), or you can always email me: taonaw&lt;at&gt;protonmail&lt;dot&gt;ch (for GPG, click the lock icon on the navbar to the left).