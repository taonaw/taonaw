+++
title = "Writing Blog Posts At Work"
author = ["JarZz"]
publishDate = 2021-11-12T00:00:00-05:00
lastmod = 2022-10-04T13:14:58-04:00
tags = ["blogging"]
draft = false
+++

I [recently installed Emacs on my work Mac](https://helpdeskheadesk.net/2021-09-28/), which allows me to access my blog's posts folder through Tramp. Even though I have my laptop with me, I discovered that the ability to quickly start a new entry in my blog posts org file does wonders to my desire to write.

<!--more-->

It doesn't make much sense at first. Taking the laptop out of my bag and going online takes perhaps a minute. But I found that consciously I can't be "all there" to write. It seems my mind finds different excuses: the keyboard on the laptop is too small and uncomfortable; the screen is too small and the sun glare from the window makes it hard to see; someone can say something in an important chat and I'll miss it because I'm not looking at the work computer.

On the other hand, the few times I convinced myself it's "OK" to open a personal file on the work machine, say because I want to check a reference from my journal, I immodestly felt paranoid. What if my screen's being recorded somehow? What if someone sees me brooding over my diary entries?

Of course this doesn't make sense. Even if my employer found a way to spy on me on my Mac and record the screen (which I don't think they would) it's them who are violating my privacy. And what are they going to find out? That I skipped my jogging session the other day? That my self-help procedure in case I get overwhelmed by "stupidity" at work is to take a walk outside? Say someone reads my screen and decides to fire me because of something I wrote. Am I _really_ getting the bad part of the deal here, or is it a good sign that there's no sense of privacy where I work?[^fn:1]

Writing a draft for a post is not such a big deal. It's convenient, I'm inside my work environment at the same time (org-mode agenda contains work and personal information), I see whatever notifications I'm getting, and I can copy-paste relevant work related experiences that I use for my posts. The dual display is big enough, the keyboard is comfortable, and I'm not losing my momentum.

This also means I'll be writing shorter posts if I write this way. I still believe I should let each post rest for at least a night before I check it again for style and spelling mistakes, but other than that, pushing out more content is crucial; otherwise it never happens.

Well, let's see if this works.


## Footnotes {#footnotes}

[^fn:1]: Just to be clear, I am not talking about a 30-minute  vent about people I hate at work. When I need more space and time for myself, work is just not the place anyway.