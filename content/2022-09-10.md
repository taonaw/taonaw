+++
title = "Emacs and a new Workflow"
author = ["JarZz"]
publishDate = 2022-09-10
lastmod = 2022-10-04T13:15:09-04:00
tags = ["emacs"]
draft = false
+++

As more of my work functions shift toward Technical Writing, I sat down to evaluate my Emacs workflow. Most of the Knowledge Base (KB) articles will be written in [ServiceNow](https://en.wikipedia.org/wiki/ServiceNow), and I thought only minor tweaks will be needed. As it turns out, the devil's in the details.

<!--more-->

I thought it would be easy to work with Emacs. My planned process looked something like this:

1.  Write a draft in org-mode.
2.  Split the draft to appropriate sections (usually: introduction, requirements, and process)
3.  Edit and check for spelling and grammar
4.  Export from Emacs to HTML, open in a browser, then copy-paste into ServiceNow.

That didn't work as planned. Titles and links did not carry over well, so I resorted to copy the HTML code itself into ServiceNow, which can import HTML code directly. That didn't work as planner either.

By default, Emacs adds extra features to HTML exports, such as table of contents, numbered headers, unique IDs nested into div tags, etc.

Most of these can be tamed by [Emacs's export settings, particularly the options](https://orgmode.org/manual/Export-Settings.html).
For example, adding `#+OPTIONS: toc:nil` at the head of the org file will remove the table of contents, and `num:nil` will remove the numbers from the headers. There are other options I need to explore, but the main problem is not with Emacs.

Once I create HTML code and import it into a KB article in ServiceNow, there's no way to apply the changes I make there _back_ into Emacs. Even if I try to make all my editing inside Emacs, other contributors might add additional content; I will then have to copy those back manually, work in Emacs, and then export them back into ServiceNow. Eck.

Knowing myself, I'll probably do this anyway. Working with Emacs is a core part of my writing and workflow. I will have to be creative and see how I can improve my workflow with time.

<span class="timestamp-wrapper"><span class="timestamp">[2022-09-18 Sun]</span></span>:
Quick additional notes after working this way for a week: it's actually not that bad.
Inside the export dispatcher in Emacs, there are options to export "body only" and "visible only" which help to narrow down unwanted additional codes further.

Another useful way to work with Emacs is to simply not import to ServiceNow as much. The draft, edits and changes can be made in Emacs, and the final product (as much as possible) is exported to a KB.


## Comments? {#comments}

Reply to this post on [Mastodon](https://mastodon.technology/@jtr/108987245845962240),[Twitter](https://twitter.com/james119847/status/1569419850073706502), or you can always email me: **taonaw &lt;at&gt; protonmail &lt;dot&gt; ch** (for GPG, click the lock icon on the navbar to the left).