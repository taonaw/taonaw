+++
title = "To Modus or Not to Modus?"
author = ["JarZz"]
publishDate = 2022-06-12
lastmod = 2022-10-04T13:15:06-04:00
tags = ["emacs"]
draft = false
+++

At some point yesterday I stumbled upon [this video from David of System Crafters](https://www.youtube.com/watch?v=JJPokfFxyFo&t=1800s) explaining how to customize the famed [Modus themes](https://github.com/protesilaos/modus-themes). I want to use these themes Ebecause they are so detailed and thoughtful, but I disagree with the main idea behind them: the sharply contrasting colors.

<!--more-->

On the other hand, the video demonstrated the depth of detail these themes go into. I haven't checked all the themes available for Emacs, but I don't doubt that there are no other themes that come close in terms of attention to detail and customization as the Modus themes. They are in a league of their own.

After watching parts of the video and browsing [the Modus themes manual](https://protesilaos.com/emacs/modus-themes), I came to the same conclusion I have before. I already have my preferred colors, and [I adjusted my theme according to these and my soft-contrast principles](https://taonaw.com/2022-01-02/).

The Modus theme did inspire me and made me consider modifying my theme further and including a light mode as well. This is a good idea especially on the laptop in well-lit rooms. Then again, the rest of my Linux theme is set to dark mode. Emacs's bright theme will stand out like a flashlight.

So do I really need it? If a certain yellow is too close to the orange somewhere in org-mode or if an emphasis somewhere is not good enough, I tweak the theme's el file to my liking. It's not pretty and the code is broken by my comments everywhere, but it works.


## Comments? {#comments}

Reply to this post on [Mastodon](https://mastodon.technology/web/@jtr/108467678699347233), or you can always email me: taonaw&lt;at&gt;protonmail&lt;dot&gt;ch (for GPG, click the lock icon on the navbar to the left).