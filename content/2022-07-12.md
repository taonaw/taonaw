+++
title = "M-x Birthday"
author = ["JarZz"]
publishDate = 2022-07-12
lastmod = 2022-10-04T13:15:08-04:00
tags = ["emacs"]
draft = false
+++

Yesterday was my Emacs anniversary. I only know that because Google Photos showed me the first picture of my journal in org-mode working exactly 4 years ago. I made this day official now. A day to be celebrated.

<!--more-->

Reflecting on my Emacs experience, I feel disbelief. Did I really teach myself how to use Emacs? Do I use it on a daily basis for hours? Is it really such a big part of my life? Well, yes.

Even though I used Linux off and on before, it's safe to say it was Emacs that got me to switch to Linux full-time. Emacs taught me to appreciate stability, familiarity, and through both of these, productivity. Before Emacs, there was [TiddlyWiki](https://en.wikipedia.org/wiki/TiddlyWiki), two years before that there was [Evernote](https://en.wikipedia.org/wiki/Evernote), and before that a collection of Google Keep notes/Docs and a lot of chaos.

When I was officially diagnosed with ADHD earlier this year it didn't come as a surprise. I remember how I couldn't keep up with my racing thoughts no matter what I tried. My system in org-mode transformed me. It upgraded me. It made me not just an organized human being, but gave me "powers." It made me the "archivist" at my work place, the person who writes out wiki articles, a source of knowledge with the patience to explain and teach. Recently, reflecting on my journal, I was able to work out difficult issues that I've been stuck in for years with a sense of confidence. Emacs is my enabler. A life-changing tool that did not only present me with a better way to do things, but _the_ way to do them.

Search through this blog's older entries, and you'll see the progression yourself. I teach myself new things all the time with [the help of wonderful people](https://mastodon.technology/about), my conclusions written in this blog, in my journal, in my daily tasks - all in Emacs org-mode.

When I started to write this post, I thought I'll write a list of things I've learned through Emacs, but the list is too long. All I have in me right now is a sense of enormous gratitude. This has been an incredible transformation.

I'm glad to be here. I love to be here.


## Comments? {#comments}

Reply to this post on [Mastodon](https://mastodon.technology/web/@jtr/108637743928672504), or you can always email me: taonaw&lt;at&gt;protonmail&lt;dot&gt;ch (for GPG, click the lock icon on the navbar to the left).


## Footnotes {#footnotes}

The idea of using M-x Birthday is borrowed from [George Jones](https://fosstodon.org/@eludom) over fosstodon Mastodon.