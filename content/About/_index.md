+++
title = "About JTR"
author = ["JarZz"]
layout = "single"
lastmod = 2022-10-04T13:15:09-04:00
draft = false
+++

JTR is a self-made-techie who works with humans and machines and enjoys interacting with both. He explores automation, privacy, and affordability in tech. As a result, he often finds himself tangled in the blurred lines of the real and the ideal.

He has broken his technology (this website included) many times, and plan to keep honoring this break-and-fix tradition going forward. While JTR feels disqualified from giving any good advice, he enjoys doing so just the same. With whatever spare time he has left, he tries to self-upgrade as he attempts to lead a semi-healthy (and somewhat sane) lifestyle.

As an individual who shuns most popular social media, JTR can be a bit tricky to find for the mainstream, but mostly approachable to the geeks. Mastodon and GPG Public key (along with an email address) is in the navbar.