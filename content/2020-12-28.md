+++
title = "The Problem with Signal"
author = ["JarZz"]
publishDate = 2020-12-29T00:00:00-05:00
lastmod = 2022-10-04T13:14:50-04:00
tags = ["privacy", "security"]
draft = false
+++

No, I don't mean the new "[hack](https://www.bbc.com/news/technology-55412230)" from Cellebrite, which still seems to make waves[^fn:1]. There's a more fundamental issue at hand, a result of Signal being secure, not because it's flawed somehow.

<!--more-->


## Privacy as Ownership {#privacy-as-ownership}

I've been thinking about privacy for a while if that's not evident from my recent blog posts. The word is so widely used these days, I feel it lost tangible meaning. What does privacy even mean anymore?

To me, privacy is the ability to make a choice regarding sharing information. This post I'm writing right now, for example. I can keep it to myself, deep in my journal pages, where it will never be shared. Or I can push it to my repository on GitLab, which will cause Netlify to pull it and build a post on it's virtual Hugo environment, with DNS pointing to my domain, for the whole world to see.

In order to have this choice, I first need to own my data. In the old days, I used to have a blog on WordPress.com. There was no local copy; I'd just log into the website, work on my draft, and publish. I never truly owned my data, despite whatever WordPress might say. They could have shut down my account without a second thought and all of my posts would be gone forever, no matter how loud I'd yell. No data ownership, no choice, no privacy[^fn:2].


## Can I Own my Data on Signal...? {#can-i-own-my-data-on-signal-dot-dot-dot}

At this point you can probably see what I'm getting at: with Signal, I don't own my data. Sure, my conversations are end-to-end encrypted. Sure, no one besides me and the person I'm talking to, not even Signal, can read these conversations. But the WordPress problem I described above still exists: I don't own the conversation.

Even though the conversation itself is stored locally in a database on my phone, it is fully encrypted (as it should be) and even if I decrypt it (in a similar manner Cellebrite did[^fn:3], it is still not laid out conveniently. It looks like lines in a table. After all, it's a database you're looking at.

As far as I know,  Signal doesn't have an option to export conversations[^fn:4]. To do so would be a security risk for sure, especially for non-informed users who just want to have an encrypted chat client on their phone. However, I believe there's room for such a feature, perhaps with limited access. Maybe for developers or folks who want to build Signal from source, which I think is still possible (Signal have their [repository on Github](https://github.com/signalapp)).


## Footnotes {#footnotes}

[^fn:1]: If you're curious if there's truth in these claims, start by reading [Signal's official response](https://signal.org/blog/cellebrite-and-clickbait/) and follow up on [Reddit](https://www.reddit.com/r/signal/comments/kiyai2/no_cellebrite_can_not_break_signal_encryption/) for a few entertaining remarks.
[^fn:2]: You could argue that having my post hosted on GitLab is not much different than WordPress, and you'd be right, with one important exception: all my posts are local before they ever get pushed to GitLab. GitLab, and the post you're now reading, are just a copy of my original content which I do my best to preserve with encrypted backups. If GitLab closes my account tomorrow, I won't lose anything. I could probably relocate to a new repository within the same day.
[^fn:3]: The retracted blog post on their site showed how they decrypted the local database on an Android device. In a way, it was pretty informative. The big problem with it was that it was no hack at all: they described a way to decrypt the information using an unlocked phone. The equivalent of breaking into a house after you already unlocked the front door with the key you happen to have.
[^fn:4]: There's an option to creak a local encrypted backup, but this backup is meant to be used with Signal. If you don't have Signal, you can't use this option.