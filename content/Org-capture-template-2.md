+++
title = "My Org Capture Templates - Part 2 emacs:"
author = ["JarZz"]
publishDate = 2019-01-21T00:00:00-05:00
lastmod = 2022-10-04T13:14:35-04:00
tags = ["emacs"]
draft = false
+++

I was happy this capture template series generated interest on [Reddit](https://www.reddit.com/r/orgmode/comments/agxe8n/my_orgmode_templates_part_one_any_interest_in_more/). One of the things I love about org-mode (and Emacs) is the passion among its users and the ability to learn more from each other. One of the major reasons for this is org-mode's complexity: with so many options, it _has_ to be personalized to its users' needs. org-mode is the most intimate tool I've used in a while. Every time I read something about someone's org-mode system, it feels like I'm invited for a discussion over a cup of coffee.

<!--more-->

Speaking of intimate, today I'm going to discuss my journal capture template(s). I hope you enjoy. As always, comments are welcome ~~on Reddit (follow me using the links above) at least until I integrate a commenting system on this site~~, or you can find me on [Mastodon](https://mastodon.technology/web/accounts/115656).


## Part 2 - The Journal template(s) {#part-2-the-journal-template--s}

First, here's the code:

```emacs-lisp
("j" "Journal" entry
  (file+datetree "~/Documents/Personal/journal.org")

  "**** %U%?%a \n" :tree-type week)
```

1.  For key-press "j" initiate "Journal" template as follows:
2.  Go to `~/Documents/Personal/journal.org` and create a [dateree](https://orgmode.org/manual/Template-elements.html#DOCF82) entry there.
3.  Create a sub-header (level 4) under the day header (level 3), under the week header (level 2), under the year header (level 1).
4.  Enter an [inactive timestamp](https://orgmode.org/manual/Creating-Timestamps.html), place the mark right after it (when done), place a link to the buffer you were just visiting.
5.  New line.
6.  Make this a _weekly_ datetree (the default is a monthly datetree).

I prefer a weekly format in my journal over a monthly format. In fact, the week is a "productivity unit" which always made sense to me, and this mindset integrates beautifully into org-mode. In my archive folder, which you saw in part 1, each week has its own `.org` file. I get a sense of content viewing the archive folder and seeing all the week files sorted by order.

The reason the timestamp and the link are condensed together (no space) is because I delete most of the timestamp and just leave the hour. This way, the mark is where I need to start erasing the timestamp from[^fn:1]. I haven't found a way yet to create an hourly timestamp (without the date) and I don't want to forget to put in the hour, so this is the best solution for now.

Why is the hour important? First, personal preference: I write the time and the location for each entry. Second, this helps me discern multiple entries when a certain event is developing, and I've journaled about it a couple of times. The hour of each entry is a separator.

Finally, I use a link to connect me back to `w[##].org` file (week number of the year, as I mentioned above) from which I created the journal entry. In my weekly org files, the incidents or events themselves contain very little details, usually only what is my next action (next thing to do) with a logbook drawer containing the times I worked on a task as I keep track. When I want to expand on something, I create a journal entry for it. I prefer to write my thoughts and conclusions in a personal space, keeping them separate from the more generic org file, which also contains work-related information. The link takes me back to that event or incident, and because the name is the same as the event, I know what I'm referring to.

{{< figure src="static/Org-capture-template-2.1.png" >}}

My journal is a feature of org-mode that fits my workflow effortlessly. The lack of friction encourages me to write my thoughts more often than I did in the past. The journal file is kept in a personal folder that is not synced to my work VM. I use TRAMP as part of the capture template on that machine, so when I want to capture something in my journal from work, I'm prompted to log into my file server with a password. This privacy barrier may sound cumbersome, but after I set it up, it works without intervention on my part. This separation of my personal files from the cloud helps me sleep better at night.


## Updates to the Journal Template: 2021-09-19 {#updates-to-the-journal-template-2021-09-19}

I wrote the above post more than two years ago. I wanted to leave the post intact, as it still explains the philosophy behind my journal well and serves as a good guide to write a functional capture template.

Here is my updated template, as it is today:

```emacs-lisp
("j" "Journal Related")
	  ("jj" "Journal: Event" entry (file+datetree+prompt "~/Personal/journal.org") "**** About: %a %^G \n\n %U:\n%?" :tree-type week)
	("je" "Journal: Elements" entry (file+datetree+prompt "~/Personal/journal.org") (file "~/Personal/temp-elements.org") :tree-type week)
	("jp" "PPP" entry
	(file "~/Personal/PPP.org")
	(file "~/Personal/temp-ppp.org"))
```

And the explanation, for the first journal template:

1.  For key-press "j" initiate "Journal Related" org-capture menu. [This is explained in this post](https://helpdeskheadesk.net/sub-menus-in-org/). Pressing J once opens the menu, and then:
2.  For another key press, J:
3.  Go to `~/Personal/journal.org`, ask me for a date, and create a [datetree](https://orgmode.org/manual/Template-elements.html#DOCF82) entry there.
4.  Create a sub-header (level 4) "About" under the day header (level 3), under the week header (level 2), under the year header (level 1).
5.  Create a link from the buffer you're on.
6.  Ask to enter a tag (in the journal, I tag entries with a person's name if it relates to them)
7.  Two new lines
8.  Make an inactive time-stamp
9.  Another new line, place the mark there.

This form of journal entry, "About: &lt;an event that happened&gt; &lt;with this person&gt;" is how I write most of my thoughts now. For example, say I watched a movie and I have a couple of thoughts to write down. I create the event itself on the agenda, a simple entry that shows me what I was doing and when. Then, I use this template to create a journal entry tied to it and write my thoughts there.

The second template has to do with [my "elements"](http://localhost:1313/help-desk-head-desk/2020-07-23/). To invoke it, I press "e" after the initial "j" that brings up the Journal Related org-capture menu. It is a template for a structured journal entry I've been writing almost every morning since the end of July 2019. The syntax for this capture template is in a file, temp-elements.org. Using a file makes it easy to deal with spaces and indents. It also helps create another sub-header inside the one I'm already creating in the template itself. Here's how the template looks like inside its file:

```emacs-lisp
**** Yesterday's Elements                                          :elements:
%^{Exercise}p %^{Food}p %^{Meditation}p %^{Sleep}p

%U

- Total: %?

***** Of Note
```

As you can see, it's just like writing the syntax into the settings above. This is something [useful I found out earlier on](https://helpdeskheadesk.net/org-capture-in-files/). As a matter of fact, I'm considering  placing most (or even all) my templates in files now, with the main settings only opening the sub-capture template and pointing to these files. This will keep things more organized.

There's a third template, the PPP one, which has to do with dating. To invoke it, I press "p" after the initial "j". I rather not get into its details here, but I promise it's similar to elements template.


## Footnotes {#footnotes}

[^fn:1]: "Future me" here: there's a solution to this problem designed directly into org capture: `%<FORMAT>`. However, since the template discussed is now obsolete, I haven't explored this further.