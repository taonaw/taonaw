+++
title = "Keeping Your Passwords (and more personal info)"
author = ["JarZz"]
publishDate = 2021-01-13T00:00:00-05:00
lastmod = 2022-10-04T13:14:50-04:00
tags = ["security", "tools"]
draft = false
+++

My first password manager was [LastPass](https://en.wikipedia.org/wiki/LastPass) and [KeePassXC](https://en.wikipedia.org/wiki/KeePassXC) could be my last. It's an excellent password manager that is built on top of KeePass which works on all major OSs, including Android. I want to spend some time on this excellent tool and some ideas to improve privacy.

<!--more-->

I first mentioned my transition to KeePass [about two years ago](https://helpdeskheadesk.net/lastpass-to-keepass/), when I discovered an issue with LastPass[^fn:1]. Looking for a free password manager option, I found [KeePassDX on F-Droid](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/) for my phone, and from there I started using KeePass on my Desktop and then KeePassXC.

The KeePass "family" keeps my passwords database on my devices (and in backups, of course) where it belongs. It's purely FOSS, which is why I'm using three different clients (KeePass for windows, KeePassDX for Android, and KeePassXC for Linux) without even realizing. They all work with each other seamlessly without issue. Of course, it's completely free and doesn't charge for any premium extensions that I use or know of.

Flexibility and customizability is not for everyone. KeePass is not the easiest to set up straight out of the box. You need a secure syncing solution (if you end up syncing your password database on something like Dropbox or Google Drive you're missing the point if you ask me) and if you want full browser integration, you need to follow some instructions and run a script to make it work. But once it works, it's beautiful. On KeePassXC You can download favicons as you set up the site's credentials. You can include lengthy notes, properties, and even attachments. You can program complicated macros that can log you into flashy dynamic log-in prompts that require delays and tab presses. It's so effective I'm using it to navigate directly to websites instead of opening Firefox first, [as I wrote two weeks ago](https://helpdeskheadesk.net/2020-12-31/).

In fact, KeePassXC and KeePassDX offer enough customizability that I can use it to store other sensitive information, like my contacts. The credit for this idea goes to Michael Bazzell and [the privacy and OSINT show](https://inteltechniques.com/blog/2020/08/02/securely-storing-contacts-within-password-managers/). With KeePassXC, I can include special properties for a phone number and address. I can include an image as an attachment and even a small mug-shot as an icon. The whole thing can be stored in contacts folders inside the database. And just like that I can have my contacts with me, anywhere I go, without needing to log into some email to retrieve an address or unlock my phone to find a number for Signal or WhatsApp. Not only this is useful (and fun!) it also keeps precious contacts information away from Google, Facebook, Apple, and the other hungry tech mongers out there who make profits out of all of our address books.

The one thing I truly don't understand is why password managers are so hard for "regular" users to use. I keep emphasizing this to friends and family, and I keep getting nonsensical answers. Sure, I don't expect everyone to go into the level of what I'm going and have a private Raspberry Pi server syncing the database between all their devices, but why not a solution like 1password or LastPass? I don't understand.


## Footnotes {#footnotes}

[^fn:1]: To be fair to LastPass, the problem exists with any browser-integrated password manager including KeePassXC. If you walk away from an unlocked machine that has an unlocked password manager, you might as well just leave your wallet with everything in it on the table. You just don't do that. I discovered another issue withing the Chrome extension specifically, but I won't get into it here. Take a look at the post if you'd like.