+++
title = "Test Post"
author = ["JarZz"]
publishDate = 2001-01-01T00:00:00-05:00
lastmod = 2021-10-17T21:48:03-04:00
draft = false
+++

This is a test post

<!--more-->

Below, using the figure shortcode with the following link:

<https://commons.wikimedia.org/wiki/Main%5FPage#/media/File:Eik%5Fmet%5Fuitgebroken%5Fkroon.%5FLocatie,%5FKroondomein%5FHet%5FLoo.%5F25-12-2020>\_(d.j.b.)\_02.jpg

{{< figure src="<https://commons.wikimedia.org/wiki/Main%5FPage#/media/File:Eik%5Fmet%5Fuitgebroken%5Fkroon.%5FLocatie,%5FKroondomein%5FHet%5FLoo.%5F25-12-2020>\_(d.j.b.)\_02.jpg" >}}

the direct link to the image using the figure shortcode:

{{< figure src="![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Eik%5Fmet%5Fuitgebroken%5Fkroon.%5FLocatie%2C%5FKroondomein%5FHet%5FLoo.%5F25-12-2020%5F%28d.j.b.%29%5F02.jpg/1024px-Eik%5Fmet%5Fuitgebroken%5Fkroon.%5FLocatie%2C%5FKroondomein%5FHet%5FLoo.%5F25-12-2020%5F%28d.j.b.%29%5F02.jpg)" >}}

{{< figure src="![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Eik%5Fmet%5Fuitgebroken%5Fkroon.%5FLocatie%2C%5FKroondomein%5FHet%5FLoo.%5F25-12-2020%5F%28d.j.b.%29%5F02.jpg/1024px-Eik%5Fmet%5Fuitgebroken%5Fkroon.%5FLocatie%2C%5FKroondomein%5FHet%5FLoo.%5F25-12-2020%5F%28d.j.b.%29%5F02.jpg)" >}}