+++
title = "The Colors of New Mexico"
author = ["JarZz"]
publishDate = 2022-01-04T00:00:00-05:00
lastmod = 2022-10-04T13:15:00-04:00
tags = ["photography", "emacs"]
draft = false
+++

I returned from a trip to Albuquerque, New Mexico, and I miss it already. My mind is still running a mental rsync against the usual daily grind, and as rsync goes with large portions of data, this can take a while. One of the things I can bring up already though is the color theme in New Mexico, which was everywhere we went. It seems to convey the colors of the desert: sand to clay browns and yellows, green-gray for the cactus, the turquoise for the sky and water.

<!--more-->

{{< figure src="images/2022-01-02.1.jpg" >}}

I grew up in a place where the desert was always nearby, and forests were small, usually limited to modest resorts of pine trees. Perhaps it was somewhat of this home-like feeling, perhaps it was the peaceful surroundings, or the way human-made structures blended into nature effortlessly. Whatever it was, I felt the colors resonate with who I am as a person, and how I operate.

{{< figure src="images/2022-01-02.2.jpg" >}}

Naturally, Emacs came to mind. Confidently, I [just discussed low contrast themes](https://www.reddit.com/r/emacs/comments/rn6qz0/low_contrast_themes_vs_high_contrast_themes/) on Reddit before the trip, and received some interesting feedback. The discussion started after I realized I enjoy low-contrast themes over high contrast[^fn:1] ones. Indeed, the colors I now use in Emacs inspired by nature where I went give me more of that soothing feeling from New Mexico.

At this point, my "slightly modified" Gotham theme, which I discussed in the Reddit post, is heavily modified. The color descriptions inside the theme's .el file don't make much sense anymore. I've also noticed that org-mode is somewhat of an afterthought in the theme, with some of its key elements not defined at all. I'm wondering how hard it would be to create my own theme based on the colors I've already added.

{{< figure src="images/2022-01-02.3.jpg" >}}

Interestingly, for such uniformity of colors, I couldn't find any official guide or even an explanation from an official source (if you happen to be from New Mexico or know of such place, please let me know!) The only thing that came close was the [New Mexico University branding guide](https://brand.unm.edu/brand-style/color-palette/index.html).


## Footnotes {#footnotes}

[^fn:1]: You can't mention Emacs themes without bringing up Prot's [Modus Themes](https://protesilaos.com/emacs/modus-themes) these days, and for a good reason. It was interesting then to be on the other end of this discussion and explain the case for low-contrast themes. If this sort of discussion interests you, check the Reddit post or feel free to write to me and tell me what your opinion is.