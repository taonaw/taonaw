+++
title = "Of TiddlyWiki, Emacs, and Digital Gardens"
author = ["jrss"]
publishDate = 2022-10-30
lastmod = 2022-10-30T17:12:15-07:00
tags = ["tools", "emacs"]
draft = false
+++

For the last couple of weeks, I've been brainstorming the idea of converting this website into a fork of a wiki, particularly, [TiddlyWiki](https://en.wikipedia.org/wiki/TiddlyWiki). I went full circle from using the tool to builing a static website, back to the original, single-file java script supported version. Here are some of my thoughts.

<!--more-->

I [brought up](https://taonaw.com/2022-08-24) digital gardens before, and again when I was [re-tagging my posts](https://taonaw.com/2022-10-14/) recently. In short, case in point: instead of having the above links leading readers to find these static discussions which are concealed with dates in the past, the discussion would be dynamic, something I keep adding to as I keep developing the idea.

To continue with this example: on this blog, I would keep discussing digital gardens until it becomes a tag and group all my posts about "digital gardens" together. You'd see a list of posts, organized by date, and you could go in and choose to read pieces based on my progress in time. This is one way to do things and it works quite well if to judge from blogs around the web.

The "digital garden" idea is to have one place, one "post" that is ever-developing, broken down into sub-topics. Certain ideas will get developed, and other abandoned in favor of others. Instead of seeing progress divided into chunks of dates, we see the progress of the idea in chunks of subtopics.

OK. Time to get this off my chest. I don't like the name "digital garden."

On the web, folks consider digital gardens anything from a dateless blog to documentations to roam and zettelkasten. I understand the metaphor of being a digital "gardener" as someone who grows ideas and articles, but I prefer to think of it as a form of emphasizing topic organization over date organization. To me, since before I created this blog, this is called a _wiki_.

The problem with wikis is that they're technical documents. Or so I thought. Wikis are named after encyclopedias, which are all about facts. But wikis are just a form of content presentation. They can be as personal as any other personal website can be, just as blogs, which are web logs, can be as technical as a changlog or as personal as an online diary.

Before I disappeared into org-mode and Emacs and left all other text editors in the dust, I enjoyed using one such wiki: [TiddlyWiki](https://tiddlywiki.com/static.html). What sets Tiddlywiki's apart besides the funny name is that it's fully self-contained: a single HTML file (at the humble size of about 3 MB when you download it first) which contains everything, text and media, by design. It is a polished tool, a more utilitarian one than eye candy, with all of its settings built into its nested articles, which are called tiddlers. You can go into its style sheets and color pallets, which are tiddlers as well, and change whatever you'd like to make it looks and behave however you'd like.

After moving to write in org-mode however, I didn't need TiddlyWiki anymore. Emacs offers a much superior writing experience, combined with org-mode, an excellent organizational system. To look into TiddlyWiki again I first had to find a way to work from within org-mode, and I couldn't find a way to do so. Without much motivation, I gave up quickly and went back to org-mode. It took the spark of going "digital garden" again to explore this a bit more in depth.

One of the first things I learned, [on mastodon](https://fosstodon.org/web/@nicksiv@octodon.social/109200639552455653) of course, was that TiddlyWiki now comes on [Node.js](https://en.wikipedia.org/wiki/Node.js). Node.js allows you to run JavaScript applications (which TiddlyWiki is based on) outside of the browser using a local server. This also means the individual tiddlers do not have to be nested inside the original HTML file; instead, every tiddler is stored as a file.

OK fine, so I have my tiddlers in tid files, what's the big deal? Well, now we have individual files with essentially text (in TiddlyWiki markup, called [wikitext](https://tiddlywiki.com/static/WikiText.html).) See where this is going?

we can now use Emacs to write and edit tiddlers using wikitext. But it gets better. As it goes with everything you learn, now I was able to ask the right questions. Looking for a way to export from org-mode to wikitext within Emacs, I found ox-tiddly on melpa, which converts org syntax to wikitext. I can now write my TiddlyWiki articles from org-mode. But it gets better even better.

through node.js functions, I also learned that you can convert TiddlyWiki into a _static website_. A few simple commands which can be made to a single script will convert all those tid files, the tiddlers which are the articles, to HTML files. TiddlyWiki can become a static website, which can be run off GitHub or Gitlab. I can even push changes through Netify, it seems. Essentially the same workflow I'm using for my current static site.

But... as I continued learning about these options, I also realized the one downside of publishing content this way, and it's a big one: no JavaScript. This means the core TiddlyWiki functions, the same ones that make it so good, won't work. There isn't even an option to navigate through the different tiddlers since the navbar is completely built on JavaScript.

There is a way to export JavaScript along with the style sheets and the tiddelrs, but then I'd still need a way to activate it. With all of this it occurred to me that it doesn't make sense to break down TiddlyWiki if I want to use it for a website; it's easier and more functional to keep it as is, a single snappy HTML file that contains everything. This is also how other TiddlyWiki websites are built, including the official tiddlywiki.com. So, while there is a slight benefit to working on individual tiddlers locally, I will need to bring everything back together before I build and push the site.

At this point, it's simpler to just copy-paste the wikitext from the temporary buffer ox-tiddly gives me and paste it into the editor. Another benefit of this method is seeing the content as I paste it live, with TiddlyWiki's built-in editor preview mode. Images and different styles that are invisible through org-mode are easier to fix this way.

I'm not happy to write outside of Emacs org-mode[^fn:1], but I do want to at least give TiddlyWiki a try. I believe the way it will present information, along with its customization, will be the next big step for this site.


## Footnotes {#footnotes}

[^fn:1]: I [discussed similar issues with writing content out of Emacs](https://taonaw.com/2022-09-10/) not too long ago, and the workarounds I've come up with. While I managed to stop using Word for the most part for now (and thankfully so), the big issue is that using Emacs is a one-way street: I can use it to publish content to other platforms (in the case TiddlyWiki), but once there, it's hard to get whatever edits I make there back in Emacs. I wish I could keep all my work in Emacs, but that's not realistic.