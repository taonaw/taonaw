+++
title = "Writing on a Daily Basis"
author = ["JarZz"]
publishDate = 2022-06-09
lastmod = 2022-10-04T13:15:06-04:00
tags = ["blogging"]
draft = false
+++

I worked out a way to write blog posts almost every day. At this point, I have more posts on the blog each month than I did before. I'm going to share what I do, but know this: it involves a lot of writing.

<!--more-->

I don't aim to write every day. As well, my recent frequent posting is _not_ intended as part of [#100DaysToOffload](https://100daystooffload.com/), which seems the popular way to commit to write on blogs these days. From the get-go, I acknowledged some days are too "crazy" and I don't have time to write, while others don't have good blogging content (this last doesn't mean I don't write at all, as I'll explain in a minute).

Since part of writing often means shorter blog posts, I used to think blogging this way more would be more or less same amount of writing as my once-in-a-while long posts. Not true. I write _a lot_ more, and a lot more often. The surprising part though is that I don't mind, and even enjoy it.

The key component was going back to write in my journal each morning instead of in the evening. I read somewhere that writing at night before bed helps with a sense of closure for the day and might help with sleep; this was mistake in my case. I was too tired and rushed my journaling, often skipping it altogether. Writing in the morning on the other hand can take up to an hour at times. I spend plenty of time on extra details and events from the previous day, and these details often becomes my posts. Journaling is a commitment in itself, but I barely feel its weight because I just have the need to write things down.

Later in the day I have a period of about 20-30 minutes writing my draft for a post. This usually means copying a part of my journal text (like this text here which will probably end up as a post on the blog) and pasting it into my Hugo capture template. I usually add some clarity around the text I just pasted from the journal (like an introduction, or a mention of a previous post of a similar topic), and edit the text itself to be more audience-friendly and less my-eyes-only information.

Still later, I polish the post a bit more. This usually means word choice, sentence structure, and removing unnecessary content. I try to hold back from deleting whole sentences and alter the content, because I won't be able to finish if I do that. Then it's time to do spelling and grammar checks ([now built into Emacs](https://taonaw.com/2022-06-07/), which helps), and then publishing the post.

As soon as I publish on the blog, I post the link to the post on Mastodon (and by [moa](https://moaparty.com/) to Twitter), then go back to my post in Emacs and add the "Comments?" section with the links to the posts on Mastodon and Twitter (if it didn't block me yet again.) I started doing that after I decided to [shut down comments](https://taonaw.com/2022-05-02/) on the blog.

All of this means about two hours of writing split throughout the day. Add to that the fact that I write plenty of notes as I work on different projects and I think it's fair to say I spend some good 4 hours writing in Emacs every day. Wow. I never realize how much this amounts to.


## Comments? {#comments}

Reply to this post on [Mastodon](https://mastodon.technology/@jtr/108447388756256502), [Twitter](https://twitter.com/James86768479/status/1534867194932301825), or you can always email me: taonaw&lt;at&gt;protonmail&lt;dot&gt;ch (for GPG, click the lock icon on the navbar to the left).